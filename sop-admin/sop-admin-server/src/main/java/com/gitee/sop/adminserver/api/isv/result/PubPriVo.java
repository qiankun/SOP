package com.gitee.sop.adminserver.api.isv.result;

import lombok.Data;

@Data
public class PubPriVo {
	private String pubKey;
	private String priKey;

}
