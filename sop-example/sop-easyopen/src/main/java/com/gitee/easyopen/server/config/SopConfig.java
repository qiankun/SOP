package com.gitee.easyopen.server.config;

import com.gitee.sop.servercommon.configuration.EasyopenServiceConfiguration;
import org.springframework.context.annotation.Configuration;

/**
 * @author tanghc
 */
@Configuration
public class SopConfig extends EasyopenServiceConfiguration {
}
