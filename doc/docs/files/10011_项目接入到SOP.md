# 项目接入到SOP

以springboot项目为例，springmvc目前暂不支持，以后可以支持。

- 工程依赖sop-service-common，和eureka

```xml
<dependency>
    <groupId>com.gitee.sop</groupId>
    <artifactId>sop-service-common</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>

<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```

- 配置文件添加eureka配置

```yaml
eureka:
  port: 1111 # eureka端口号
  host: localhost # eureka地址
  client:
    serviceUrl:
      defaultZone: http://${eureka.host}:${eureka.port}/eureka/
```

- 在springboot启动类上添加`@EnableDiscoveryClient`
- 新增一个配置类，继承`AlipayServiceConfiguration.java`，内容为空

```java
@Configuration
public class OpenServiceConfig extends AlipayServiceConfiguration {

}
```

到此准备工作就完成了，接下来可前往`新增接口`查看如何新增接口。
