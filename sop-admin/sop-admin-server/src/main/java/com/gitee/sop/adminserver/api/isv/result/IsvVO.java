package com.gitee.sop.adminserver.api.isv.result;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author thc
 */
@Data
public class IsvVO {
    /**  数据库字段：id */
    private Long id;

    /** appKey, 数据库字段：app_key */
    private String appKey;

    /** secret, 数据库字段：secret */
    private String secret;

    /** 公钥, 数据库字段：pub_key */
    private String pubKey;

    /** 私钥, 数据库字段：pri_key */
    private String priKey;

    /** 0启用，1禁用, 数据库字段：status */
    private Byte status;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;

    private List<RoleVO> roleList;
}
